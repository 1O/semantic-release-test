# [1.8.0](https://gitlab.com/1O/semantic-release-test/compare/v1.7.0...v1.8.0) (2022-01-31)


### Features

* added new build step ([8105e7d](https://gitlab.com/1O/semantic-release-test/commit/8105e7deba15a864ef3e22dfb9f4808a012a296e))

# [1.7.0](https://gitlab.com/1O/semantic-release-test/compare/v1.6.3...v1.7.0) (2022-01-31)


### Features

* new docker image ([f276cc0](https://gitlab.com/1O/semantic-release-test/commit/f276cc0a259ad89accdc01baeff491127dcaef75))

## [1.6.3](https://gitlab.com/1O/semantic-release-test/compare/v1.6.2...v1.6.3) (2022-01-31)


### Bug Fixes

* added release tag ([2744796](https://gitlab.com/1O/semantic-release-test/commit/2744796217817aa26272bbcfcb213f8a24a4160c))

## [1.6.2](https://gitlab.com/1O/semantic-release-test/compare/v1.6.1...v1.6.2) (2022-01-31)


### Bug Fixes

* failed login ([bd14842](https://gitlab.com/1O/semantic-release-test/commit/bd14842ce50162ffc679553737ca02f9a0ef1afb))

## [1.6.1](https://gitlab.com/1O/semantic-release-test/compare/v1.6.0...v1.6.1) (2022-01-31)


### Bug Fixes

* new release ([51f3cb2](https://gitlab.com/1O/semantic-release-test/commit/51f3cb23b6356c343980825b481693557ee909c8))

# [1.6.0](https://gitlab.com/1O/semantic-release-test/compare/v1.5.0...v1.6.0) (2022-01-30)


### Bug Fixes

* renamed build stage ([5c05247](https://gitlab.com/1O/semantic-release-test/commit/5c05247d377fe087acc879727d1d49dd6bcf1e8a))


### Features

* Release Docker ([d612101](https://gitlab.com/1O/semantic-release-test/commit/d61210193f20e600ce2eaff920c17701cb72cb2d))

# [1.5.0](https://gitlab.com/1O/semantic-release-test/compare/v1.4.2...v1.5.0) (2022-01-30)


### Features

* changed release message ([520c67a](https://gitlab.com/1O/semantic-release-test/commit/520c67a4137481d6c56aeb1cb9da3892f55ccdd6))

## [1.4.2](https://gitlab.com/1O/semantic-release-test/compare/v1.4.1...v1.4.2) (2021-09-18)


### Bug Fixes

* use package for assets ([10beff5](https://gitlab.com/1O/semantic-release-test/commit/10beff5a0a563143cfae8d58296e5e9d75ce9074))

## [1.4.1](https://gitlab.com/1O/semantic-release-test/compare/v1.4.0...v1.4.1) (2021-09-18)


### Bug Fixes

* added leading slash to asset filepath ([1bfc139](https://gitlab.com/1O/semantic-release-test/commit/1bfc139186994dba6b3d0f539a150f05ccee7b75))

# [1.4.0](https://gitlab.com/1O/semantic-release-test/compare/v1.3.2...v1.4.0) (2021-09-18)


### Bug Fixes

* added zip ([3bf9408](https://gitlab.com/1O/semantic-release-test/commit/3bf94087de5e1b5d9f2d8362bded8e8042f0b995))


### Features

* added zip release ([9db986f](https://gitlab.com/1O/semantic-release-test/commit/9db986f8feff6b46a44b07ea917e0204a7fc554c))

## [1.3.2](https://gitlab.com/1O/semantic-release-test/compare/v1.3.1...v1.3.2) (2021-09-18)


### Bug Fixes

* git assets ([f96aa91](https://gitlab.com/1O/semantic-release-test/commit/f96aa91e24a390f4523bb7df38634cd3fddc95f5))
