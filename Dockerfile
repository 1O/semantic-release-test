ARG ARCH=
FROM ${ARCH}alpine:3.10.0

RUN addgroup -S app && adduser -S -G app app

RUN mkdir /app \
    && chown -R app:app /app

COPY version.sh /app

CMD [ "/app/version.sh" ]