#!/bin/sh

if [ $# -ne 1 ]; then exit 1; fi

TAG=$1

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
docker login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_PASSWORD" "$DOCKERHUB_REGISTRY"

docker buildx build \
    --push \
    --platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
    --tag "$CI_REGISTRY_IMAGE:$TAG" \
    --tag "$DOCKERHUB_USER/$DOCKERHUB_REPO:$TAG" .
