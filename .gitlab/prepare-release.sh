#!/bin/sh

if [ $# -ne 1 ]; then exit 1; fi

NEW_VERSION=$1

sed -Ei "s/(VERSION=).*/\1${NEW_VERSION}/" version.cmd
sed -Ei "s/(VERSION=).*/\1${NEW_VERSION}/" version.sh

zip release-"${NEW_VERSION}.zip" version.cmd version.sh README.md